#!/usr/bin/env python
# coding: utf-8

# # Linear Regression

# Linear regression is an approach to model the relationship between a single dependent variable (target variable) and one (simple regression) or more (multiple regression) independent variables. The linear regression model assumes a linear relationship between the input and output variables. If this relationship is present, we can estimate the coefficients required by the model to make predictions on new data.

# In[1]:


import pandas as pd
df=pd.read_csv(r'/home/aarush100616/Downloads/Projects/Linear Regression/weight-height.csv')
df


# In[3]:


df.shape


# In[4]:


df.info()


# In[5]:


df.dtypes


# In[6]:


df.Gender.nunique()


# In[7]:


df.Gender.unique()


# In[9]:


import matplotlib.pyplot as plt
plt.style.use('ggplot')
# histogram of the height
df.Height.plot(kind='hist', color='purple', edgecolor='black', figsize=(10,7))
plt.title('Distribution of Height', size=24)
plt.xlabel('Height (inches)', size=18)
plt.ylabel('Frequency', size=18)


# In[10]:


# histogram of the weight
df.Weight.plot(kind='hist', color='purple', edgecolor='black', figsize=(10,7))
plt.title('Distribution of Weight', size=24)
plt.xlabel('Weight (pounds)', size=18)
plt.ylabel('Frequency', size=18);


# In[11]:


# histogram of the height - males and females
df[df['Gender'] == 'Male'].Height.plot(kind='hist', color='blue', edgecolor='black', alpha=0.5, figsize=(10, 7))
df[df['Gender'] == 'Female'].Height.plot(kind='hist', color='magenta', edgecolor='black', alpha=0.5, figsize=(10, 7))
plt.legend(labels=['Males', 'Females'])
plt.title('Distribution of Height', size=24)
plt.xlabel('Height (inches)', size=18)
plt.ylabel('Frequency', size=18);


# In[12]:


# histogram of the weight - males and females
df[df['Gender'] == 'Male'].Weight.plot(kind='hist', color='blue', edgecolor='black', alpha=0.5, figsize=(10, 7))
df[df['Gender'] == 'Female'].Weight.plot(kind='hist', color='magenta', edgecolor='black', alpha=0.5, figsize=(10, 7))
plt.legend(labels=['Males', 'Females'])
plt.title('Distribution of Weight', size=24)
plt.xlabel('Weight (pounds)', size=18)
plt.ylabel('Frequency', size=18);


# In[13]:


# Descriptive statistics male
statistics_male = df[df['Gender'] == 'Male'].describe()
statistics_male.rename(columns=lambda x: x + '_male', inplace=True)
# Descriptive statistics female
statistics_female = df[df['Gender'] == 'Female'].describe()
statistics_female.rename(columns=lambda x: x + '_female', inplace=True)
# Dataframe that contains statistics for both male and female
statistics = pd.concat([statistics_male, statistics_female], axis=1)
statistics


# In[14]:


# Scatter plot of Height and Weight
ax1 = df[df['Gender'] == 'Male'].plot(kind='scatter', x='Height', y='Weight', color='blue', alpha=0.5, figsize=(10, 7))
df[df['Gender'] == 'Female'].plot(kind='scatter', x='Height', y='Weight', color='magenta', alpha=0.5, figsize=(10 ,7), ax=ax1)
plt.legend(labels=['Males', 'Females'])
plt.title('Relationship between Height and Weight', size=24)
plt.xlabel('Height (inches)', size=18)
plt.ylabel('Weight (pounds)', size=18);


# In[15]:


# Scatter plot of 500 females
sample_females = df[df['Gender'] == 'Female'].sample(500)
sample_females.plot(kind='scatter', x='Height', y='Weight', color='magenta', alpha=0.5, figsize=(10, 7))
plt.legend(labels=['Females'])
plt.title('Relationship between Height and Weight (sample of 500 females)', size=20)
plt.xlabel('Height (inches)', size=18)
plt.ylabel('Weight (pounds)', size=18);


# ## Simple Linear Regression (SLR)

# Simple linear regression is a linear approach to modeling the relationship between a dependent variable and an independent variable, obtaining a line that best fits the data.
# (y=a+bx)where x is the independent variable (height), y is the dependent variable (weight), b is the slope, and a is the intercept. The intercept represents the value of y when x is 0 and the slope indicates the steepness of the line. The objective is to obtain the line that best fits our data (the line that minimize the sum of square errors). The error is the difference between the real value y and the predicted value y_hat, which is the value obtained using the calculated linear equation.
# {error = y(real)-y(predicted) = y(real)-(a+bx)}

# In[16]:


import numpy as np
# best fit polynomials
df_males = df[df['Gender'] == 'Male']
df_females = df[df['Gender'] == 'Female']
# polynomial - males
male_fit = np.polyfit(df_males.Height, df_males.Weight, 1)
# polynomial - females
female_fit = np.polyfit(df_females.Height, df_females.Weight, 1)


# In[17]:


# scatter plots and regression lines
# males and females dataframes
df_males = df[df['Gender'] == 'Male']
df_females = df[df['Gender'] == 'Female']
# Scatter plots.
ax1 = df_males.plot(kind='scatter', x='Height', y='Weight', color='blue', alpha=0.5, figsize=(10, 7))
df_females.plot(kind='scatter', x='Height', y='Weight', color='magenta', alpha=0.5, figsize=(10, 7), ax=ax1)
# regression lines
plt.plot(df_males.Height, male_fit[0] * df_males.Height + male_fit[1], color='darkblue', linewidth=2)
plt.plot(df_females.Height, female_fit[0] * df_females.Height + female_fit[1], color='deeppink', linewidth=2)
# regression equations
plt.text(65, 230, 'y={:.2f}+{:.2f}*x'.format(male_fit[1], male_fit[0]), color='darkblue', size=12)
plt.text(70, 130, 'y={:.2f}+{:.2f}*x'.format(female_fit[1], female_fit[0]), color='deeppink', size=12)
# legend, title and labels.
plt.legend(labels=['Males Regresion Line', 'Females Regresion Line', 'Males', 'Females'])
plt.title('Relationship between Height and Weight', size=24)
plt.xlabel('Height (inches)', size=18)
plt.ylabel('Weight (pounds)', size=18);


# ### With seaborn

# In[18]:


import seaborn as sns
# regression plot using seaborn
fig = plt.figure(figsize=(10, 7))
sns.regplot(x=df_males.Height, y=df_males.Weight, color='blue', marker='+')
sns.regplot(x=df_females.Height, y=df_females.Weight, color='magenta', marker='+')
# Legend, title and labels.
plt.legend(labels=['Males', 'Females'])
plt.title('Relationship between Height and Weight', size=24)
plt.xlabel('Height (inches)', size=18)
plt.ylabel('Weight (pounds)', size=18);


# In[19]:


import seaborn as sns
# 300 random samples
df_males_sample = df[df['Gender'] == 'Male'].sample(300)
df_females_sample = df[df['Gender'] == 'Female'].sample(300)
# regression plot using seaborn
fig = plt.figure(figsize=(10, 7))
sns.regplot(x=df_males_sample.Height, y=df_males_sample.Weight, color='blue', marker='+')
sns.regplot(x=df_females_sample.Height, y=df_females_sample.Weight, color='magenta', marker='+')
# legend, title, and labels.
plt.legend(labels=['Males', 'Females'])
plt.title('Relationship between Height and Weight', size=24)
plt.xlabel('Height (inches)', size=18)
plt.ylabel('Weight (pounds)', size=18);


# ## Fitting a simple linear model using sklearn

# In[20]:


from sklearn.linear_model import LinearRegression
df_males = df[df['Gender'] == 'Male']
# create linear regression object
lr_males = LinearRegression()
# fit linear regression
lr_males.fit(df_males[['Height']], df_males['Weight'])
# get the slope and intercept of the line best fit
print(lr_males.intercept_)
print(lr_males.coef_)
df_females = df[df['Gender'] == 'Female']
# create linear regression object
lr_females = LinearRegression()
# fit linear regression
lr_females.fit(df_females[['Height']], df_females['Weight'])
# get the slope and intercept of the line best fit
print(lr_females.intercept_)
print(lr_females.coef_)


# In[21]:


df_females = df[df['Gender'] == 'Female']
# fit the model using numpy
female_fit = np.polyfit(df_females.Height, df_females.Weight, 1)
# predictions using numpy
print(np.polyval(female_fit, [60]))
# fit the model using scikit learn
lr_females = LinearRegression()
lr_females.fit(df_females[['Height']], df_females['Weight'])
# predictions using scikit learn
print(lr_females.predict([[60]]))


# ## Pearson correlation coefficient

# In[22]:


# dataframe containing only females
df_females = df[df['Gender'] == 'Female']
# correlation coefficients 
df_females.corr()


# In[23]:


# dataframe containing only males
df_males = df[df['Gender'] == 'Male']
# correlation coefficients 
df_males.corr()


# A rule of thumb for interpreting the size of the correlation coefficient is the following:
# 
#     1–0.8 → Very strong
#     0.799–0.6 → Strong
#     0.599–0.4 → Moderate
#     0.399–0.2 → Weak
#     0.199–0 → Very Weak

# ## Pearson using scipy

# In[24]:


from scipy import stats
# dataframe containing only females
df_females = df[df['Gender'] == 'Female']
# pearson correlation coefficient and p-value
pearson_coef, p_value = stats.pearsonr(df_females.Height, df_females.Weight)
print(pearson_coef)
# dataframe containing only males
df_males = df[df['Gender'] == 'Male']
# pearson correlation coefficient and p-value
pearson_coef, p_value = stats.pearsonr(df_males.Height, df_males.Weight)
print(pearson_coef)


# ## Residual plots

# In[25]:


import seaborn as sns
# dataframe containing only females
df_females = df[df['Gender'] == 'Female'].sample(500)
# residual plot 500 females
fig = plt.figure(figsize = (10, 7))
sns.residplot(df_females.Height, df_females.Weight, color='magenta')
# title and labels
plt.title('Residual plot 500 females', size=24)
plt.xlabel('Height (inches)', size=18)
plt.ylabel('Weight (pounds)', size=18);


# In[26]:


# dataframe containing only males
df_males = df[df['Gender'] == 'Male'].sample(500)
# residual plot 500 males
fig = plt.figure(figsize=(10, 7))
sns.residplot(df_males.Height, df_males.Weight, color='blue')
# title and labels
plt.title('Residual plot 500 males', size=24)
plt.xlabel('Height (inches)', size=18)
plt.ylabel('Weight (pounds)', size=18);


# As we can see, the points are randomly distributed around 0, meaning linear regression is an appropriate model to predict our data. If the residual plot presents a curvature, the linear assumption is incorrect. In this case, a non-linear function will be more suitable to predict the data.

# ## Multiple linear regression

# Multiple linear regression uses a linear function to predict the value of a target variable y, containing the function n independent variable x=[x₁,x₂,x₃,…,xₙ].
#     y =b ₀+b ₁x ₁+b₂x₂+b₃x₃+…+bₙxₙ
# We obtain the values of the parameters bᵢ, using the same technique as in simple linear regression (least square error). After fitting the model, we can use the equation to predict the value of the target variable y. In our case, we use height and gender to predict the weight of a person Weight = f(Height,Gender).

# Categorical variables in multiple linear regression
# There are two types of variables used in statistics: numerical and categorical variables.
#     Numerical variables represent values that can be measured and sorted in ascending and descending order such as the height of a person.
#     Categorical variables are values that can be sorted in groups or categories such as the gender of a person.

# In[31]:


import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns


# In[32]:


dataset = pd.read_csv("/home/aarush100616/Downloads/Projects/Linear Regression/advertising.csv")


# In[33]:


dataset


# In[34]:


#Setting the value for X and Y
x = dataset[['TV', 'Radio', 'Newspaper']]
y = dataset['Sales']


# In[37]:


#Splitting the dataset
from sklearn.model_selection import train_test_split
x_train, x_test, y_train, y_test = train_test_split(x, y, test_size = 0.3, random_state = 100)


# In[38]:


#Fitting the Multiple Linear Regression model
mlr = LinearRegression()  
mlr.fit(x_train, y_train)


# In[39]:


#Intercept and Coefficient
print("Intercept: ", mlr.intercept_)
print("Coefficients:")
list(zip(x, mlr.coef_))


# In[40]:


#Prediction of test set
y_pred_mlr= mlr.predict(x_test)


# In[41]:


#Predicted values
print("Prediction for test set: {}".format(y_pred_mlr))


# In[44]:


#Actual value and the predicted value
mlr_diff = pd.DataFrame({'Actual value': y_test, 'Predicted value':
                         y_pred_mlr})
mlr_diff.head()


# In[45]:


#Model Evaluation
from sklearn import metrics
meanAbErr = metrics.mean_absolute_error(y_test, y_pred_mlr)
meanSqErr = metrics.mean_squared_error(y_test, y_pred_mlr)
rootMeanSqErr = np.sqrt(metrics.mean_squared_error(y_test, y_pred_mlr))
print('R squared: {:.2f}'.format(mlr.score(x,y)*100))
print('Mean Absolute Error:', meanAbErr)
print('Mean Square Error:', meanSqErr)
print('Root Mean Square Error:', rootMeanSqErr)


# ## Conclusion
# The Multiple Linear Regression model performs well as 90.11% of the data fit the regression model. Also, the mean absolute error, mean square error, and the root mean square error are less.
